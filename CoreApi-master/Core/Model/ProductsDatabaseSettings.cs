﻿namespace XperCore.Model
{
    public class ProductsDatabaseSettings
    {
        public string ProductsCollectionName { get; set; }

        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
