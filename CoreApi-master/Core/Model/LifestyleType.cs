﻿namespace XperCore.Model
{
    public enum LifestyleType
    {
        Kosher,
        Vegan,
        Vegetarian,
        Organic,
        LowSugar,
        LowSalt
    }
}
